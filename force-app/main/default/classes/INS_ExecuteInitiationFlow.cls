public with sharing class INS_ExecuteInitiationFlow {
  @InvocableMethod(
    label='Execute Initiation Flow'
    description='Executes a flow based on the initiation action on Insight_Type'
  )
  public static void execute(List<ID> insightIds) {
    List<Insight__c> insights = [
      SELECT
        Id,
        Contact__c,
        Account__c,
        Payload_JSON__c,
        Insight_Type__r.Initiation_Flow__c
      FROM Insight__c
      WHERE Id IN :insightIds
    ];
    for (Insight__c insight : insights) {
      Map<String, Object> inputs = new Map<String, Object>();
      inputs.put('recordId', insight.Id);
      if (!String.isBlank(insight.Contact__c)) {
        inputs.put('contactId', insight.Contact__c);
      }

      if (!String.isBlank(insight.Account__c)) {
        inputs.put('accountId', insight.Account__c);
      }
      //TODO: map payload to inputs
      if (!String.isBlank(insight.Payload_JSON__c)) {
        try {
          Map<String, Object> payload = (Map<String, Object>) JSON.deserializeUntyped(
            insight.Payload_JSON__c
          );
          inputs.putAll(payload);
        } catch (Exception ex) {
          //TODO:  handle:  ignore for prototype
        }
      }

      if (!String.isBlank(insight.Insight_Type__r.Initiation_Flow__c)) {
        Flow.Interview launchedFlow = Flow.Interview.createInterview(
          insight.Insight_Type__r.Initiation_Flow__c,
          inputs
        );
        launchedFlow.start();
      }
    }
  }
}
