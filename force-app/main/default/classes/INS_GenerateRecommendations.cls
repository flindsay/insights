global with sharing class INS_GenerateRecommendations {

@InvocableMethod(
	label='Recommendation Generator from Insights'
	       description='Generate recommendations from insights.')
global static List<List<Recommendation> > getRecommendations(List<RecommendationGenerateRequest> inputRequests){
	List<List<Recommendation> > outputs = new List<List<Recommendation> >();

	for (RecommendationGenerateRequest inputRequest : inputRequests)
	{
		List<Insight__c> insights = [SELECT id, Name, Nominal_Value__c, insight_type__c, details__c, insight_type__r.execution_flow__c FROM insight__c WHERE Account__c = :inputRequest.accountId and insight_type__r.execution_flow__c !=null and Status__c ='Open'];
		List<Recommendation> recs = new List<Recommendation>();
		for (Insight__c insight:insights) {
			Recommendation rec = new Recommendation(
				Name = insight.Name,
				Description = insight.details__c,
				ActionReference = insight.insight_type__r.execution_flow__c,
                Insight__c = insight.Id,
				AcceptanceLabel = 'Accept',
                RejectionLabel = 'No Thanks',
                Nominal_Value__c = insight.Nominal_Value__c,
				Payload = insight.Payload__c

				);
			recs.add(rec);
		}
		outputs.add(recs);
	}
	return outputs;
}

global class RecommendationGenerateRequest {
@InvocableVariable(label='Account ID')
global String accountId;
}
}

